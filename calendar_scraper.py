import pandas as pd
import sys
from datetime import datetime
import datetime as dt
import dateparser
import os
sys.path.append('./code/')
from calendar_class import courtCalendar
from upload_class import uploadData
# from update_watch_list import updateWatchList

# kings1, kings2, kings_nuisance or queens
court = 'kings2'

# format: "YYYY-MM-DD"
start_date = "2021-3-7"
end_date = "2021-3-10"

# put the path to calendar_html_temp here, if the default doesn't work

dir_ = os.getcwd() + "/calendar_html_temp/"

if __name__ == "__main__":

    now_ = dt.datetime.now()
    if (now_ - dateparser.parse(start_date)).days > 13:
        print('invalid start date ... resetting')
        date_ = (now_ - dt.timedelta(days=14))
        start_date = date_.strftime("%Y-%m-%d")
        print(start_date)
    court_data = courtCalendar(court, dir_, start_date, end_date)
    if sys.argv[-1] != 'p':
        court_data.getData()
        court_data.downloadCalendarData()
    else:
        print('parsing only!')

    uploader = uploadData(dir_)
    uploader.parseProcessAndUpload()
    uploader.updateRunTracker(start_date, end_date, court)




