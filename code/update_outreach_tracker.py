import json
import os
import sys

from google.oauth2 import service_account
from googleapiclient.discovery import build
import pandas as pd

from upload_class import uploadData


GSHEET_ID = '1UI-bikpJU5Msymaq-nLCOL6lAoeO96fY1cbaJAezkbQ'
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']


DB_QUERY = """
with appearance_info as (
	-- for some reason adding group by messes up the window functions
	select 
		a.case_index,
		GROUP_CONCAT(
			concat(date(a.appearance_datetime), '; ', a.part, '; ', a.outcome) 
			order by a.appearance_datetime desc
			separator '\n'
		) as appearances_info
	from appearances as a
	group by case_index	
), atty_marshal_info as (
	select 
		case_index,
		case 
			when defendant_atty = '' then concat('No as of ', cast(scraped_datetime as date))
			else 'Yes'
		end as has_atty,
		case
			when marshal = 'none' then concat('No as of ', cast(scraped_datetime as date)) 
			else 'Yes'
		end as marshal_assigned,
		case
			when case_found = 1 then 'found'
			when case_found = 0 then 'not found'
			else ''
		end as case_found
	from case_updates
	group by case_index
)
select distinct
	t.case_index, 
	t.tenant_address, 
	t.tenant_name, 
	case when
		t.docket_id in ('not found', 'DOCKET_ID_MISSING') then null
		else concat('https://iapps.courts.state.ny.us/nyscef/DocumentList?docketId=', t.docket_id, '&display=all')
	end as case_info_link,
	ai.appearances_info,
	FIRST_VALUE(date(a.appearance_datetime)) OVER (PARTITION BY case_index ORDER BY a.appearance_datetime desc) AS lateset_appearance,
	FIRST_VALUE(a.part) OVER (PARTITION BY case_index ORDER BY a.appearance_datetime desc) AS latest_part,
	FIRST_VALUE(a.outcome) OVER (PARTITION BY case_index ORDER BY a.appearance_datetime desc) AS lateset_outcome,
	am.has_atty,
	am.marshal_assigned,
	am.case_found
FROM tenants as t
inner join appearances as a using(case_index)
inner join appearance_info as ai using(case_index)
left join atty_marshal_info as am using(case_index)
where t.case_index like '%%/KI';
"""


def main(argv):
    service_account_credentials = os.getenv('SHEETS_CREDENTIALS')
    if not service_account_credentials:
        raise ValueError('invalid SHEETS_CREDENTIALS, check your environment')

    credentials = service_account.Credentials.from_service_account_info(
        json.loads(service_account_credentials), scopes=SCOPES)

    service = build('sheets', 'v4', credentials=credentials,
                    cache_discovery=False)

    engine = uploadData('.').makeEngine()
    data = pd.read_sql(DB_QUERY, engine)
    data['lateset_appearance'] = data.lateset_appearance.astype(str)

    body = {'values': [data.columns.tolist()] + data.values.tolist()}
    result = service.spreadsheets().values().update(
        spreadsheetId=GSHEET_ID, range='data-import!B:L',
        valueInputOption="RAW", body=body).execute()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
